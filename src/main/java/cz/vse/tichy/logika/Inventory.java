
package cz.vse.tichy.logika;

import java.util.ArrayList;
/**
 *  Class Inventory - Manages players inventory.
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class Inventory {
    private ArrayList<Item> items = new ArrayList<Item>(); //definition of Inventory with size 10
    public int maxInventorySpace = 10;
    /**
     * Method checks if there is space in inventory, if its possible the item is added
     *
     *@param item Item to be inserted.
     *@return true or false depending on success of insert
     */
    public boolean insertIntoInventory (Item item) {
        if (maxInventorySpace > items.size()){ //if there is space in inventory Item can be added
            items.add(item);
            return true;
        }
        else {
            return false;
        }

    }
    /**
     * This is getter for items
     *
     *@return All items
     */
    public ArrayList<Item> getItems() {
        return items;
    }
}
