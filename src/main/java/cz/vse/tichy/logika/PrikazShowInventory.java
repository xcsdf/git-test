package cz.vse.tichy.logika;

import java.util.ArrayList;
/**
 *  Class Inventory - Lists all items from inventory.
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class PrikazShowInventory implements IPrikaz {
    private static final String NAZEV = "inventory";
    private HerniPlan plan;
    /**
     * Creates command inventory.
     *
     *@param  plan
     */
    public PrikazShowInventory(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     *Lists all items in inventory
     *
     *@param parametry
     *@return item name in variable tmp
     */
    @Override
    public String provedPrikaz(String... parametry) {
        ArrayList<Item> itemstmp = this.plan.getInventory().getItems(); //select all items in inventory and print them
        String tmp = "You have: \n";
        for (int i = 0; i<itemstmp.size(); i++){
            tmp += itemstmp.get(i).getName() + "\n";
        }

        return tmp;
    }
    /**
     * get name of command
     *
     *@return name of command
     */
    @Override
    public String getNazev() {
        return NAZEV;

    }


}
