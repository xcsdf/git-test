package cz.vse.tichy.logika;

import java.util.ArrayList;
/**
 *  Class PickUp - its for picking up items.**
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class PrikazPickUp implements IPrikaz {
    private static final String NAZEV = "pickup";
    private HerniPlan plan;
    /**
     * Creates command pickup.
     *
     *@param  plan
     */
    public PrikazPickUp (HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Pickups item from room
     *
     *@param parametry
     *@return different strings depending on success of operation
     */
    @Override
    public String provedPrikaz(String... parametry) {
        ArrayList<Item> itemstmp = this.plan.getAktualniProstor().getItems();//Checks what items are in this room
        if (parametry.length == 0){
            return "Please write valid item name";
        }
        for (int i = 0; i<itemstmp.size(); i++){                              //checks if item is in room and if its movable
            if (itemstmp.get(i).getName().equals(parametry[0])){
                if (itemstmp.get(i).getMovable()) {
                    if (this.plan.getInventory().insertIntoInventory(itemstmp.get(i))){//add item to inventory, i is for cycle which looks in room
                        if (! itemstmp.get(i).getMultiplePickup()){
                            itemstmp.remove(i);
                        }
                        return "Done you picked up the item";
                    }
                    else{
                        return "Your inventory is full";
                    }

                }
                else{
                    return "This item is not movable";
                }
            }
        }

        return "Item not found";
    }

    /**
     * get name of command
     *
     *@return name of command
     */
    @Override
    public String getNazev() {
        return NAZEV;

    }
}
