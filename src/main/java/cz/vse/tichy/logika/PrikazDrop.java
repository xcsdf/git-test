package cz.vse.tichy.logika;

import java.util.ArrayList;
/**
 *  Class Inventory - Drops item from your inventory into current room.
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class PrikazDrop implements IPrikaz{
    private static final String NAZEV = "drop";
    private HerniPlan plan;
    /**
     * Creates command drop.
     *
     *@return itemName
     */
    public PrikazDrop(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Drops selected item from inventory
     * cycle checks
     * @param parametry
     *@return success or error message
     */
    @Override
    public String provedPrikaz(String... parametry) { //removes item from inventory and adds it to current room
        ArrayList<Item> itemstmp = this.plan.getInventory().getItems();
        for (int i = 0; i<itemstmp.size(); i++){
            if (itemstmp.get(i).getName().equals(parametry[0])){ //check if the item we want to remove is in room
                String dropedItem = itemstmp.get(i).getName();
                plan.getAktualniProstor().addItem(itemstmp.get(i));
                itemstmp.remove(i); //removes item from inventory
                return "you have droped " + dropedItem;
            }

        }

        return "This item could not be dropped";
    }
    /**
     * Get name of command.
     *
     *@return command name
     */
    @Override
    public String getNazev() {
        return NAZEV;

    }
}
