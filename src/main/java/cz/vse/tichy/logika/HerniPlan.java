package cz.vse.tichy.logika;


/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan {

    private Inventory inventory;
    private Prostor aktualniProstor;

    public Inventory getInventory() {
        return inventory;
    }

    /**
     *  Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        zalozProstoryHry();
        inventory = new Inventory();

    }
    /**
     *  Vytváří jednotlivé prostory a propojuje je pomocí východů.
     *  Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {
        // vytvářejí se jednotlivé prostory
        Prostor beach = new Prostor("Beach","Beach\nBeach you were stranded on");
        Prostor jungle = new Prostor("Jungle", "Jungle\n Deep dark and full of sound, you dont want to go too far");
        Prostor cave = new Prostor("Cave","Cave\n Damp cave with waterfall in it.\n You see something behind it, some secret pathway maybe?");
        Prostor oldFortress = new Prostor("OldFortress","Old fortress\n mostly rubble now but the view is really nice");
        Prostor secretPathway = new Prostor("SecretPathway","SecretPathway\n You feel proud that you were able to find this");
        Prostor trialRoom = new Prostor("TrialRoom","Trial room\n Here you will be tested if you are worthy of the pirate gold.\n Type puzzle to start");

        oldFortress.addItem(new Item("Cannonball",true, false));
        oldFortress.addItem(new Item("DeadSeagull", false,false));
        oldFortress.addItem(new Item("SpiderWeb",false, true));
        jungle.addItem(new Item("Bannana", true,true));
        jungle.addItem(new Item("Coconut", true,true));
        beach.addItem(new Item("Seashell", true,true));
        jungle.addItem(new Item("Monkey",false,false));

        secretPathway.lock();


        // set exits to rooms
        beach.setVychod(jungle);
        beach.setVychod(oldFortress);
        beach.setVychod(cave);
        jungle.setVychod(beach);
        oldFortress.setVychod(beach);
        cave.setVychod(beach);
        cave.setVychod(secretPathway);
        secretPathway.setVychod(cave);
        secretPathway.setVychod(trialRoom);
        trialRoom.setVychod(secretPathway);

        aktualniProstor = beach;  // game starts on beach
    }

    
    /**
     *  Metoda vrací odkaz na aktuální prostor, ve ktetém se hráč právě nachází.
     *
     *@return     aktuální prostor
     */
    
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    
    /**
     *  Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     *@param  prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
       aktualniProstor = prostor;
    }

}
