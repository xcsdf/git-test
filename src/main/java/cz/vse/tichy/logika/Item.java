package cz.vse.tichy.logika;
/**
 *  Class Item - Represent an item you can pickup in game.
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class Item {
    private String itemName;
    private boolean movable;
    private boolean multiplePickup;
    /**
     * Constructor creates new item.
     *
     *@param itemName,movable,multiplePickup
     *@return true or false depending on success of insert
     */
    public Item(String itemName, boolean movable, boolean multiplePickup) {
        this.itemName = itemName;
        this.movable = movable;
        this.multiplePickup = multiplePickup;
    }
    /**
     * name of item.
     *
     *@return itemName
     */
    public String getName() {
       return itemName;
    } //definition of item and things around items
    /**
     *  if item is movable.
     *
     *@return if it is movable
     */
    public boolean getMovable() {
        return movable;
    }
    /**
     * Can it be picked up multiple times.
     *
     *@return if it  can be picked up multiple times.
     */
    public boolean getMultiplePickup() {
        return multiplePickup;
    }

}

