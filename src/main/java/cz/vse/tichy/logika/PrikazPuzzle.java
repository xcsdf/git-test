package cz.vse.tichy.logika;

/**
 *  Class Inventory - Tries to solve puzzle in current room, does nothing if there is no puzzle in room.
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class PrikazPuzzle implements IPrikaz {
    private static final String NAZEV = "puzzle";
    private HerniPlan plan;
    private Hra game;
    /**
     * Creates command puzzle.
     *
     *@param  plan,game
     */
    public PrikazPuzzle(HerniPlan plan,Hra game) {
        this.plan = plan;
        this.game = game;

    }
    /**
     * Tries to solve the puzzle in the room
     *
     *@param parametry
     *@return Different strings depending on outcome of this command
     */
    @Override
    public String provedPrikaz(String... parametry) {  //if the player is in Cave and has Cannonball item the Secret pathway will be unlocked otherwise the hint will be displayed
       if(this.plan.getAktualniProstor().getNazev().equals("Cave")){
           for (int i = 0; i<this.plan.getInventory().getItems().size(); i++) {
               if (this.plan.getInventory().getItems().get(i).getName().equals("Cannonball")) {
                   Prostor neighboringRoom = plan.getAktualniProstor().vratSousedniProstor("SecretPathway");
                   neighboringRoom.unlock();
                   this.plan.getInventory().getItems().remove(i);
                   return "You have completed the puzzle, and thus new room has been unlocked";
               }
           }
           return "You seem to be missing some heavy object";
       }
       if(this.plan.getAktualniProstor().getNazev().equals("TrialRoom")){ //if the player is in Trial room this puzzle is displayed
           if (parametry.length == 0){
               return "You need to chose one of the cups \n First is made of wood and without any decorations\n" +
                       "Second is made of bronze with nice carving \n" +
                       "Third is made of gold with gems all over it\n" +
                       "Choose the right cup and remember it's the pirate treasure you are looking for\n" +
                       "Type puzzle and number of chosen cup (1,2,3)";
           }
           else {
               if (parametry[0].equals("3")){
                   this.game.setKonecHry(true);

                   return "You have chosen well \n" +
                           "the treasure room stands open before you";//if you pick right you win.
               }
               else if (parametry[0].equals("1")){
                   this.game.setKonecHry(true);
                   return "you suddenly feel light headed and week in knees.You have been cursed and fall dead";
               }
               else if (parametry[0].equals("2")){
                   this.game.setKonecHry(true);
                   return "you suddenly feel light headed and week in knees.You have been cursed and fall dead";
               }
               else return "You are supposed to pick 1,2 or 3";


           }

       }
       return "You are not in room with puzzle, so there is nothing to do"; //last resort for puzzle command when player is not in room with puzzle
    }
    /**
     * get name of command
     *
     *@return name of command
     */
    @Override
    public String getNazev() {
        return NAZEV;

    }
}
