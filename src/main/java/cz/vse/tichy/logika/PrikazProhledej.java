package cz.vse.tichy.logika;

import java.util.ArrayList;
/**
 *  Class Inventory - Searches room for items.
 *@author     Jakub Tichy
 *@version    1.0.7
 *@created    2020-05-06
 */
public class PrikazProhledej implements IPrikaz {

    private static final String NAZEV = "search";
    private HerniPlan plan;
    /**
     * Creates command search.
     *
     *@param  plan
     */
    public PrikazProhledej(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Returns all items found in room.
     *
     *@param parametry
     *@return Vrací celé číslo s nejvyšší hodnotou
     */
    @Override
    public String provedPrikaz(String... parametry) {  //program will check what is suposed to be in certain room and display it
        ArrayList<Item> itemstmp = this.plan.getAktualniProstor().getItems();
        String tmp = "You have found: \n";
        for (int i = 0; i<itemstmp.size(); i++){
            tmp += itemstmp.get(i).getName() + ". Can that be moved? " + itemstmp.get(i).getMovable()+ " \n"; //shows what item was found and if it can be moved
        }

        return tmp;
    }
    /**
     * get name of command
     *
     *@return name of command
     */
    @Override
    public String getNazev() {
        return NAZEV;

    }
}
