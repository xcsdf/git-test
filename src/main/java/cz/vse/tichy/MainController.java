package cz.vse.tichy;

import cz.vse.tichy.logika.Hra;
import cz.vse.tichy.logika.IHra;
import cz.vse.tichy.logika.Item;
import cz.vse.tichy.logika.Prostor;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.xml.soap.Text;
import java.io.InputStream;
import java.util.Collection;
import java.util.Optional;

public class MainController {
    public TextArea textOutput;
    public TextField textInput;
    private IHra hra;

    public Label locationName;
    public Label locationDescription;

    public VBox exits;
    public VBox items;
    public VBox map;

    public HBox inventory;
    public Pane roompic;

    /**
     *
     * @param hra new game
     * Start of game
     */
    public void init(IHra hra) {
        this.hra = hra;
        update();
    }

    /**
     * Metoda vola ostatni updaty
     */
    private void update() {
        String location = getAktualniProstor().getNazev();
        locationName.setText(location);

       String description = getAktualniProstor().dlouhyPopisBezVychodu();
        locationDescription.setText(description);

        updateExits();
        updateItems();
        updateMap();
        updateInventory();
        updateRoompic();
    }

    /**
     * update items
     * returns items in room
     */
    private void updateItems() {
        Collection<Item> itemList = getAktualniProstor().getItems();
        items.getChildren().clear();

        for (Item item : itemList) {
            String itemName = item.getName();
            Label itemLabel = new Label(itemName);
            InputStream stream = getClass().getClassLoader().getResourceAsStream(itemName + ".jpg");
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            itemLabel.setGraphic(imageView);

            if(item.getMovable()) {
                itemLabel.setCursor(Cursor.HAND);

                itemLabel.setOnMouseClicked(event -> {
                    executeCommand("pickup "+itemName);
                });
            } else {
                itemLabel.setTooltip(new Tooltip("This item is not movable"));
            }

            items.getChildren().add(itemLabel);
        }

    }

    /**
     * Updates exits from current room
     */
    private void updateExits() {
        Collection<Prostor> exitList = getAktualniProstor().getVychody();
        exits.getChildren().clear();

        for (Prostor prostor : exitList) {
            String exitName = prostor.getNazev();
            Label exitLabel = new Label(exitName);
            exitLabel.setCursor(Cursor.HAND);
            exitLabel.setTooltip(new Tooltip(prostor.dlouhyPopis()));

            InputStream stream = getClass().getClassLoader().getResourceAsStream(exitName + ".jpg");
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            exitLabel.setGraphic(imageView);

            exitLabel.setOnMouseClicked(event -> {
                executeCommand("go "+exitName);
            });

            exits.getChildren().add(exitLabel);
        }
    }

    /**
     *Executes comands
     * @param command
     */
    private void executeCommand(String command) {
        String result = hra.zpracujPrikaz(command);
        textOutput.appendText(result + "\n\n");
        update();
    }

    /**
     * returns current room
     * @return
     */
    private Prostor getAktualniProstor() {
        return hra.getHerniPlan().getAktualniProstor();
    }

    /**
     * Method is called if user types something into textField if the character is ENTER method executeCommand is called.
     * @param keyEvent action when fieldbox registers keystroke
     */
    public void onInputKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            executeCommand(textInput.getText());
            textInput.setText("");
        }
    }

    /**
     * This method starts new game. With cleared text field and inventory.
     */
    public void StartNewGame() {
        this.hra = new Hra();
        textOutput.clear();
        update();
    }

    /**
     * Method calls the map picture with current room in red circle.
     */
    private void updateMap() {
        map.getChildren().clear();
        Label exitLabel = new Label();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("map-" + getAktualniProstor().getNazev() + ".jpg");
        Image img = new Image(stream);
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(240);
        imageView.setFitHeight(240);
        exitLabel.setGraphic(imageView);
        map.getChildren().add(exitLabel);

    }

    /**
     * Method calls in the picture of current room
     */
    private void updateRoompic() {
        roompic.getChildren().clear();
        Label exitLabel = new Label();
        InputStream stream = getClass().getClassLoader().getResourceAsStream( getAktualniProstor().getNazev() + ".jpg");
        Image img = new Image(stream);
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(400);
        imageView.setFitHeight(400);
        exitLabel.setGraphic(imageView);
        roompic.getChildren().add(exitLabel);

    }

    /**
     * This method updates inventory, with adding items. When user clicks on icon the item is droped
     */
    private void updateInventory() {
        Collection<Item> itemList = this.hra.getHerniPlan().getInventory().getItems();
        inventory.getChildren().clear();

        for (Item item : itemList) {
            String itemName = item.getName();
            Label itemLabel = new Label(itemName);
            InputStream stream = getClass().getClassLoader().getResourceAsStream(itemName + ".jpg");
            Image img = new Image(stream);
            ImageView imageView = new ImageView(img);
            imageView.setFitWidth(60);
            imageView.setFitHeight(40);
            itemLabel.setGraphic(imageView);
            itemLabel.setCursor(Cursor.HAND);

            itemLabel.setOnMouseClicked(event -> {
                executeCommand("drop "+itemName);
                });


            inventory.getChildren().add(itemLabel);
        }

    }

    /**
     * This method is for opening help HTML document
     */
    public void help() {
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load( getClass().getResource("/Help.html").toString() );
        Scene scene = new Scene(webView,600,600);
        Stage newWindow = new Stage();
        newWindow.setTitle("Help");
        newWindow.setScene(scene);

        newWindow.show();
    }

    /**
     * This method is for end game button. The dialog window will open and choice can be made.
     */
    public void EndGame() {
        textOutput.setText(textOutput.getText().concat("\nClose dialog is on active screen"));
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Exit dialog");
        alert.setHeaderText("Do you really want to quit");
        alert.setContentText("Are you sure about this?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK){
            textOutput.setText(textOutput.getText().concat("\nUser pressed OK"));
            alert.close();
            Platform.exit();
        } else {
            textOutput.setText(textOutput.getText().concat("\nUser pressed cancel"));
            alert.close();

        }


    }

}

