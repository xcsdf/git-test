package cz.vse.tichy.logika;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra
 *
 * @author    Jarmila Pavlíčková
 * @version  pro školní rok 2016/2017
 */
public class HraTest {


    //== Datové atributy (statické i instancí)======================================

    //== Konstruktory a tovární metody =============================================
    //-- Testovací třída vystačí s prázdným implicitním konstruktorem ----------

    //== Příprava a úklid přípravku ================================================

    /***************************************************************************
     * Metoda se provede před spuštěním každé testovací metody. Používá se
     * k vytvoření tzv. přípravku (fixture), což jsou datové atributy (objekty),
     * s nimiž budou testovací metody pracovat.
     */
    @Before
    public void setUp() {

    }


    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každé testovací metody.
     */
    @After
    public void tearDown() {
    }

    //== Soukromé metody používané v testovacích metodách ==========================

    //== Vlastní testovací metody ==================================================

    /***************************************************************************
     * Testuje průběh hry, po zavolání každěho příkazu testuje, zda hra končí
     * a v jaké aktuální místnosti se hráč nachází.
     * Při dalším rozšiřování hry doporučujeme testovat i jaké věci nebo osoby
     * jsou v místnosti a jaké věci jsou v batohu hráče.
     * 
     */
    @Test
    public void testPrubehHry() {
        Hra hra1 = new Hra();
        assertEquals("Beach", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("go OldFortress");
        assertFalse( hra1.konecHry());
        assertEquals("OldFortress", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("pickup Cannonball");
        hra1.zpracujPrikaz("go Beach");
        assertFalse( hra1.konecHry());
        assertEquals("Beach", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("go Cave");
        assertFalse( hra1.konecHry());
        assertEquals("Cave", hra1.getHerniPlan().getAktualniProstor().getNazev());
        assertTrue(hra1.getHerniPlan().getAktualniProstor().vratSousedniProstor("SecretPathway").locked());
        hra1.zpracujPrikaz("puzzle");
        assertFalse(hra1.getHerniPlan().getAktualniProstor().vratSousedniProstor("SecretPathway").locked());
        hra1.zpracujPrikaz("go SecretPathway");
        assertFalse( hra1.konecHry());        assertEquals("SecretPathway", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("go TrialRoom");
        assertFalse( hra1.konecHry());        assertEquals("TrialRoom", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("puzzle 3");
        assertTrue(hra1.konecHry());
        hra1.zpracujPrikaz("konec");
        assertTrue(hra1.konecHry());
    }
    @Test
    public void testInventory(){
        Hra hra1 = new Hra();
        assertEquals("Beach", hra1.getHerniPlan().getAktualniProstor().getNazev());
        for (int i =0; i<hra1.getHerniPlan().getInventory().maxInventorySpace;i++) {
            hra1.zpracujPrikaz("pickup Seashell");
        }
        assertEquals(hra1.getHerniPlan().getInventory().maxInventorySpace,hra1.getHerniPlan().getInventory().getItems().size());
        hra1.zpracujPrikaz("pickup Seashell");
        assertEquals(hra1.getHerniPlan().getInventory().maxInventorySpace,hra1.getHerniPlan().getInventory().getItems().size());
    }
    @Test
    public void testMovable(){
        Hra hra1 = new Hra();
        assertEquals("Beach", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("go Jungle");
        assertFalse(hra1.konecHry());
        assertEquals("Jungle", hra1.getHerniPlan().getAktualniProstor().getNazev());
        hra1.zpracujPrikaz("pickup Monkey");
        assertEquals(0,hra1.getHerniPlan().getInventory().getItems().size());

    }
}
